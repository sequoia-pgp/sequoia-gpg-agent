//! Test data for Sequoia.
//!
//! This module includes the test data from `tests/data` in a
//! structured way.

use std::fmt;
use std::collections::BTreeMap;

pub struct Test {
    path: &'static str,
    #[allow(dead_code)]
    pub bytes: &'static [u8],
}

impl fmt::Display for Test {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "tests/data/{}", self.path)
    }
}

/// Returns the content of the given file below `openpgp/tests/data`.
pub fn file(name: &str) -> &'static [u8] {
    use std::sync::OnceLock;

    static FILES: OnceLock<BTreeMap<&'static str, &'static [u8]>>
        = OnceLock::new();
    FILES.get_or_init(|| {
        let mut m: BTreeMap<&'static str, &'static [u8]> =
            Default::default();

        macro_rules! add {
            ( $key: expr, $path: expr ) => {
                m.insert($key, include_bytes!($path))
            }
        }
        include!(concat!(env!("OUT_DIR"), "/tests.index.rs.inc"));

        // Sanity checks.
        assert!(m.contains_key("keys/testy-new.pgp"));
        assert!(m.contains_key("keys/testy-new-private.pgp"));
        m
    }).get(name).unwrap_or_else(|| panic!("No such file {:?}", name))
}

/// Returns the content of the given file below `tests/data/keys`.
pub fn key(name: &str) -> &'static [u8] {
    file(&format!("keys/{}", name))
}
