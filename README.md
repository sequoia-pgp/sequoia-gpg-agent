This crate includes functionality for interacting with GnuPG's
`gpg-agent`.

`gpg-agent` is a secret key store, which is shipped as part of GnuPG.
It is used to manage secret key material, and hardware devices that
contain secret key material.  It provides an RPC interface using the
[Assuan protocol].

  [Assuan protocol]: https://gnupg.org/software/libassuan

This is how `gpg`, GnuPG's primary command-line interface,
communicates with it.

This crate provides a Rust API for interacting with `gpg-agent`.

Note: this crate communicates directly with `gpg-agent`; it does not
go via `gpg`.
